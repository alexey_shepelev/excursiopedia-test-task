# README #

This README document whatever steps are necessary to get application up and running.

### How do I get set up? ###

* cd ./excursiopedia-test-task/
* cp config/application.yml.example config/application.yml
* cp config/database.yml.example config/database.yml
* cp config/secrets.yml.example config/secrets.yml
* rake db:create
* rake db:migrate
* rake db:load
